package com.example.parrot.pong1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.constraint.solver.widgets.Rectangle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // -----------------------------------
    // ## ANDROID DEBUG VARIABLES
    // -----------------------------------

    // Android debug variables
    final static String TAG="PONG-GAME";

    // -----------------------------------
    // ## SCREEN & DRAWING SETUP VARIABLES
    // -----------------------------------

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;



    // -----------------------------------
    // ## GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------

    Point ballPosition;
    final int Ball_width=45;

    Point paddlePosition;

    // variables for controlling size of paddle
    final int distance_from_botom=350;
    final int paddle_width=200;
    final int paddle_height=20;


    // ----------------------------
    // ## GAME STATS - number of lives, score, etc
    // ----------------------------
int score=0;

    public GameEngine(Context context, int w, int h) {
        super(context);


        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;


        this.printScreenInfo();

        // @TODO: Add your sprites to this section
        // This is optional. Use it to:
        //  - setup or configure your sprites
        //  - set the initial position of your sprites


        //initial position of ball
        ballPosition=new Point();
        ballPosition.x=this.screenWidth/2;
        ballPosition.y=this.screenHeight/2;


        //initial position of paddle
        paddlePosition=new Point();
        paddlePosition.x=(this.screenWidth/2)-paddle_width;
        paddlePosition.y=(this.screenHeight-paddle_height-distance_from_botom);



        // @TODO: Any other game setup stuff goes here


    }

    // ------------------------------
    // HELPER FUNCTIONS
    // ------------------------------

    // This funciton prints the screen height & width to the screen.
    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }


    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    boolean movingDown=true;
    boolean movingRight=true;

    final int ball_speed=45;
    final int paddle_speed=20;

    // 1. Tell Android the (x,y) positions of your sprites
    public void updatePositions() {
        // @TODO: Update the position of the sprites

        if(movingDown==true)
        {
            ballPosition.y=ballPosition.y+ball_speed;


        }

        else{
            ballPosition.y=ballPosition.y-ball_speed;
        }
        // @TODO: Collision detection code
        if(ballPosition.y>screenHeight)
        {
            movingDown=false;
        }
        if(ballPosition.y<0){
            movingDown=true;
            this.score=score+1;

        }

        Log.d(TAG, "Ball position"+ballPosition.y);

     /*   if(movingRight==true)
        {
            paddlePosition.x=paddlePosition.x+paddle_speed;


        }

        else{
            paddlePosition.x=paddlePosition.x-paddle_speed;
        }
        // @TODO: Collision detection code
        if(paddlePosition.x>screenWidth)
        {
            movingRight=false;
        }
        if(paddlePosition.x<0){
            movingRight=true;


        }
        */

    }













    // 2. Tell Android to DRAW the sprites at their positions
    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------
            // Put all your drawing code in this section

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,0,0,255));
            paintbrush.setColor(Color.WHITE);


            //@TODO: Draw the sprites (rectangle, circle, etc)

            int left=ballPosition.x;
            int top=ballPosition.y;
            int right=ballPosition.x+Ball_width;
            int bottom=ballPosition.y+Ball_width;
            canvas.drawRect(left,top,right,bottom,paintbrush);



            //draw paddle
            int paddleLeft=paddlePosition.x;
            int paddleTop=paddlePosition.y;
            int paddleRight=paddlePosition.x+2*paddle_width;
            int paddleBottom=paddlePosition.y+paddle_height;
            canvas.drawRect(paddleLeft,paddleTop,paddleRight,paddleBottom,paintbrush);

            canvas.drawText("Score"+this.score,20,100,paintbrush);
            //@TODO: Draw game statistics (lives, score, etc)
            paintbrush.setTextSize(60);


            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    // Sets the frame rate of the game
    public void setFPS() {
        try {
            gameThread.sleep(50);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            // user pushed down on screen

            if(event.getX()<this.screenWidth/2){
                paddlePosition.x=paddlePosition.x-paddle_speed;
            }
            else{
                paddlePosition.x=paddlePosition.x+paddle_speed;
            }
        }
        else if (userAction == MotionEvent.ACTION_UP) {
            // user lifted their finger
        }
        return true;
    }
}